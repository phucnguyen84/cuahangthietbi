package com.example.cuhngthietbi.model;

public class GioHang {
    public int id,sl;
    public long gia;
    public String ten,hinh;

    public GioHang(int id, int sl, long gia, String ten, String hinh) {
        this.id = id;
        this.sl = sl;
        this.gia = gia;
        this.ten = ten;
        this.hinh = hinh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSl() {
        return sl;
    }

    public void setSl(int sl) {
        this.sl = sl;
    }

    public long getGia() {
        return gia;
    }

    public void setGia(long gia) {
        this.gia = gia;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }
}
