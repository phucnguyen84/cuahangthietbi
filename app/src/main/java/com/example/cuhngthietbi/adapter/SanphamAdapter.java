package com.example.cuhngthietbi.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cuhngthietbi.R;
import com.example.cuhngthietbi.activity.ChiTietSanPham;
import com.example.cuhngthietbi.model.SanPham;
import com.example.cuhngthietbi.ultil.CheckConnection;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class SanphamAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<SanPham> arraySanPham;
    private int VIEW_TYPE_FLIPPER = 0, VIEW_TYPE_PRODUCT = 1;

    public SanphamAdapter(Context context, ArrayList<SanPham> arraySanPham) {
        this.context = context;
        this.arraySanPham = arraySanPham;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<SanPham> getArraySanPham() {
        return arraySanPham;
    }

    public void setArraySanPham(ArrayList<SanPham> arraySanPham) {
        this.arraySanPham = arraySanPham;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //khoi tao view da khoi tao
        if(viewType == VIEW_TYPE_FLIPPER){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dong_viewflipper,null);
            return new ItemViewFlipperHolder(v);
        }else if(viewType == VIEW_TYPE_PRODUCT){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dong_sanphammoinhat,null);
            return new ItemHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ItemViewFlipperHolder){
            ItemViewFlipperHolder viewHolder = (ItemViewFlipperHolder) holder;
            viewHolder.onFill();
        }else if(holder instanceof ItemHolder){
            ItemHolder viewHolder = (ItemHolder) holder;
            SanPham sanpham = arraySanPham.get(position-1);
            viewHolder.txttensanpham.setText(sanpham.getTensanpham());
            DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
            viewHolder.txtgiasanpham.setText("Giá: " + decimalFormat.format(sanpham.getGiasanpham())+ " Đ");
            Picasso.get().load(sanpham.getHinhanhsanpham()).into(viewHolder.imghinhsanpham);//change
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0){
            return VIEW_TYPE_FLIPPER;
        }else if(position>=1){
            return VIEW_TYPE_PRODUCT;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return arraySanPham.size()+1;
    }

    class ItemHolder extends RecyclerView.ViewHolder
    {
        public ImageView imghinhsanpham;
        public TextView txttensanpham,txtgiasanpham;


        public ItemHolder( View itemView) {
            super(itemView);
            imghinhsanpham = itemView.findViewById(R.id.img_sanpham);
            txttensanpham = itemView.findViewById(R.id.tv_tensanpham);
            txtgiasanpham = itemView.findViewById(R.id.tv_giasanpham);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = new Intent(context, ChiTietSanPham.class);
                    it.putExtra("thongtinsanpham",arraySanPham.get(getAdapterPosition()-1));
                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    CheckConnection.showToast_Short(context,arraySanPham.get(getAdapterPosition()-1).getTensanpham());
                    context.startActivity(it);
//                    Intent intentChitietsp = new Intent(context, ChiTietSanPham.class);
//                    intentChitietsp.putExtra("thongtinsanpham", arraySanPham.get(getPosition()));
//                    context.startActivity(intentChitietsp);

                }
            });

        }
    }

    class ItemViewFlipperHolder extends RecyclerView.ViewHolder {
        ViewFlipper viewFlipper;
        String image = "https://cdn.tgdd.vn/2020/06/banner/Combo-800-300-800x300.png";
        String image2="https://cdn.tgdd.vn/2020/06/banner/S20-800-300-800x300.png";
        String image3="https://cdn.tgdd.vn/2020/05/banner/800-300-800x300-23.png";
        public ItemViewFlipperHolder(@NonNull View itemView) {
            super(itemView);
            viewFlipper = itemView.findViewById(R.id.lipper);
        }

        public void onFill() {
            if(CheckConnection.haveNetworkConnection(itemView.getContext())){
                ActionViewLipper();
            }
            else{
                CheckConnection.showToast_Short(itemView.getContext(),"CHECK CONNECT");
            }
        }
        private void ActionViewLipper() {
            ArrayList<String> mangAds = new ArrayList<>();
            mangAds.add(image);
            mangAds.add(image2);
            mangAds.add(image3);
            //View lipper nhan vao imageView => gan vao imageView
            for(int i = 0 ; i < mangAds.size() ; i++){
                ImageView imageView = new ImageView(itemView.getContext());
                Picasso.get().load(mangAds.get(i)).into(imageView);//change
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);//set vua du vs lipper cua minh
                viewFlipper.addView(imageView);

            }
            viewFlipper.setFlipInterval(3000);
            viewFlipper.setAutoStart(true);
            Animation animation_in_right = AnimationUtils.loadAnimation(itemView.getContext(),R.anim.slide_in_right);
            Animation animation_out_right = AnimationUtils.loadAnimation(itemView.getContext(),R.anim.slide_out_right);
            viewFlipper.setInAnimation(animation_in_right);
            viewFlipper.setOutAnimation(animation_out_right);



        }
    }


}
