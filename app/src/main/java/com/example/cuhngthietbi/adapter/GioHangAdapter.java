package com.example.cuhngthietbi.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.example.cuhngthietbi.R;
//import com.example.cuhngthietbi.activity.GioHang;
import com.example.cuhngthietbi.activity.MainActivity;
import com.example.cuhngthietbi.model.GioHang;
import com.squareup.picasso.Picasso;


import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class GioHangAdapter extends BaseAdapter {
    Context context;
    ArrayList<GioHang> gh;

    public GioHangAdapter(Context context, ArrayList<GioHang> gh) {
        this.context = context;
        this.gh = gh;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<GioHang> getGh() {
        return gh;
    }

    public void setGh(ArrayList<GioHang> gh) {
        this.gh = gh;
    }

    @Override
    public int getCount() {
        return gh.size();
    }

    @Override
    public Object getItem(int position) {
        return gh.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class ViewHolder {
        public TextView tvtengiohang,tvgiagiohang;
        public ImageView imggiohang;
        public TextView btnminus,btnplus,btnvalues;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(view == null){
            viewHolder = new ViewHolder();
            //gan layout
            LayoutInflater layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=layoutInflater.inflate(R.layout.dong_giohang,null);
            viewHolder.tvtengiohang=view.findViewById(R.id.tv_tengiohang);
            viewHolder.tvgiagiohang=view.findViewById(R.id.tv_giagiohang);
            viewHolder.imggiohang=view.findViewById(R.id.img_giohang);
            viewHolder.btnminus=view.findViewById(R.id.btn_minus);
            viewHolder.btnplus=view.findViewById(R.id.btn_plus);
            viewHolder.btnvalues=view.findViewById(R.id.btn_values);
            view.setTag(viewHolder);


        }else{
            viewHolder= (ViewHolder) view.getTag();
        }
        GioHang gioHang = (GioHang) getItem(position);
        viewHolder.tvtengiohang.setText(gioHang.getTen());
        DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
        viewHolder.tvgiagiohang.setText(decimalFormat.format(gioHang.getGia())+" VNĐ");
        Picasso.get().load(gioHang.getHinh()).into(viewHolder.imggiohang);
        viewHolder.btnvalues.setText(gioHang.getSl()+"");

        //
        int sl = Integer.parseInt(viewHolder.btnvalues.getText().toString());
        if(sl >=10){
            viewHolder.btnplus.setVisibility(view.INVISIBLE);
            viewHolder.btnminus.setVisibility(view.VISIBLE);

        }
        else if (sl<=1){
            viewHolder.btnminus.setVisibility(view.INVISIBLE);

        }
        else if (sl>=1){
            viewHolder.btnplus.setVisibility(view.VISIBLE);
            viewHolder.btnminus.setVisibility(view.VISIBLE);
        }
//        final ViewHolder finalViewHolder = viewHolder;
//        viewHolder.btnplus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int slmsnhat = Integer.parseInt(finalViewHolder.btnvalues.getText().toString())+1;
//                int slht = MainActivity.arrCart.get(position).getSl();
//                long giaht = MainActivity.arrCart.get(position).getGia();
//                MainActivity.arrCart.get(position).setSl(slmsnhat);
//                long giamsnhat = (giaht* slmsnhat)/slht;
//                MainActivity.arrCart.get(position).setGia(giamsnhat);
//                DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
//                finalViewHolder.tvgiagiohang.setText(decimalFormat.format(giamsnhat)+"VNĐ");
//                com.example.cuhngthietbi.activity.GioHang.catchEvent();
//                if(slmsnhat > 9){
//
//                    finalViewHolder.btnplus.setVisibility(View.INVISIBLE);
//                    finalViewHolder.btnminus.setVisibility(View.VISIBLE);
//                    finalViewHolder.btnvalues.setText(String.valueOf(slmsnhat));
//
//                }else {
//                    finalViewHolder.btnplus.setVisibility(View.VISIBLE);
//                    finalViewHolder.btnminus.setVisibility(View.VISIBLE);
//                }
//            }
//        });
//        viewHolder.btnminus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int slmsnhat = Integer.parseInt(finalViewHolder.btnvalues.getText().toString())-1;
//                int slht = MainActivity.arrCart.get(position).getSl();
//                long giaht = MainActivity.arrCart.get(position).getGia();
//                MainActivity.arrCart.get(position).setSl(slmsnhat);
//                long giamsnhat = (giaht* slmsnhat)/slht;
//                MainActivity.arrCart.get(position).setGia(giamsnhat);
//                DecimalFormat decimalFormat=new DecimalFormat("###,###,###");
//                finalViewHolder.tvgiagiohang.setText(decimalFormat.format(giamsnhat)+"VNĐ");
//                com.example.cuhngthietbi.activity.GioHang.catchEvent();
//                if(slmsnhat < 2){
//
//                    finalViewHolder.btnminus.setVisibility(View.INVISIBLE);
//                    finalViewHolder.btnplus.setVisibility(View.VISIBLE);
//                    finalViewHolder.btnvalues.setText(String.valueOf(slmsnhat));
//
//                }else {
//                    finalViewHolder.btnplus.setVisibility(View.VISIBLE);
//                    finalViewHolder.btnminus.setVisibility(View.VISIBLE);
//                }
//            }
//        });

//
        final ViewHolder finalViewHolder = viewHolder;
        final ViewHolder finalViewHolder1 = viewHolder;
        viewHolder.btnplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int slmoinhat = Integer.parseInt(finalViewHolder.btnvalues.getText().toString()) + 1;
                int slhientai = MainActivity.arrCart.get(position).getSl();
                long giaht = MainActivity.arrCart.get(position).getGia();
                MainActivity.arrCart.get(position).setSl(slmoinhat);
                long giamoinhat = (giaht * slmoinhat) / slhientai;
                MainActivity.arrCart.get(position).setGia(giamoinhat);
                DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
                finalViewHolder.tvgiagiohang.setText(decimalFormat.format(giamoinhat)+ " Đ");
                com.example.cuhngthietbi.activity.GioHang.catchEvent();
                if (slmoinhat > 9){
                    finalViewHolder1.btnplus.setVisibility(View.INVISIBLE);
                    finalViewHolder1.btnminus.setVisibility(View.VISIBLE);
                    finalViewHolder.btnvalues.setText(String.valueOf(slmoinhat));
                }else {
                    finalViewHolder.btnminus. setVisibility(View.VISIBLE);
                    finalViewHolder.btnplus. setVisibility(View.VISIBLE);
                    finalViewHolder.btnvalues.setText(String.valueOf(slmoinhat));
                }
            }
        });
        //
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final AlertDialog.Builder buidlder=new AlertDialog.Builder(context);
                buidlder.setMessage("Bạn có chắc chắn muốn xóa sản phẩm này không ?");
                buidlder.setIcon(android.R.drawable.ic_delete);
                buidlder.setTitle("Xác nhận xóa");
                buidlder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                buidlder.setPositiveButton("Xác nhận", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        MainActivity.arrCart.remove(position);
                        com.example.cuhngthietbi.activity.GioHang.gioHangAdapter.notifyDataSetChanged();
                        if(MainActivity.arrCart.size()==0){
                            com.example.cuhngthietbi.activity.GioHang.tvThongbao.setVisibility(View.VISIBLE);
                        }
                        long tong=0;
                        for(int i=0;i<MainActivity.arrCart.size();i++){
                            tong+=MainActivity.arrCart.get(i).getGia();
                        }
                        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
                        com.example.cuhngthietbi.activity.GioHang.tvTongtien.setText(decimalFormat.format(tong)+ " VNĐ");
                    }
                });
                AlertDialog alertDialog=buidlder.create();
                alertDialog.show();
                return false;
            }
        });

        //
        viewHolder.btnminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int slmoinhat = Integer.parseInt(finalViewHolder.btnvalues.getText().toString()) - 1;
                int slhientai = MainActivity.arrCart.get(position).getSl();
                long giaht = MainActivity.arrCart.get(position).getGia();
                MainActivity.arrCart.get(position).setSl(slmoinhat);
                long giamoinhat = (giaht * slmoinhat) / slhientai;
                MainActivity.arrCart.get(position).setGia(giamoinhat);
                DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
                finalViewHolder.tvgiagiohang.setText(decimalFormat.format(giamoinhat)+ " Đ");
                com.example.cuhngthietbi.activity.GioHang.catchEvent();
                if (slmoinhat < 2){
                    finalViewHolder.btnminus.setVisibility(View.INVISIBLE);
                    finalViewHolder.btnplus.setVisibility(View.VISIBLE);
                    finalViewHolder.btnvalues.setText(String.valueOf(slmoinhat));
                }else {
                    finalViewHolder.btnminus. setVisibility(View.VISIBLE);
                    finalViewHolder.btnplus. setVisibility(View.VISIBLE);
                    finalViewHolder.btnvalues.setText(String.valueOf(slmoinhat));
                }
            }
        });
        //



        return view;
    }
}
