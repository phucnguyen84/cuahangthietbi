package com.example.cuhngthietbi.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cuhngthietbi.R;
import com.example.cuhngthietbi.model.SanPham;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class LaptopAdapter extends BaseAdapter {
    Context context;
    ArrayList<SanPham> arrayLap;

    public LaptopAdapter(Context context, ArrayList<SanPham> arrayLap) {
        this.context = context;
        this.arrayLap = arrayLap;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<SanPham> getArrayPhone() {
        return arrayLap;
    }

    public void setArrayPhone(ArrayList<SanPham> arrayPhone) {
        this.arrayLap = arrayPhone;
    }

    @Override
    public int getCount() {
        return arrayLap.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayLap.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }
    public class ViewHolder{
        public TextView txttenlaptop,txtgialt,txtmotalaptop;
        public ImageView imglt;


    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(viewHolder ==  null){/// sencond not load
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.dong_laptop,null);
            viewHolder.txttenlaptop = view.findViewById(R.id.tv_laptop);
            viewHolder.txtgialt = view.findViewById(R.id.tv_gialaptop);
            viewHolder.txtmotalaptop = view.findViewById(R.id.tv_motalaptop);
            viewHolder.imglt = view.findViewById(R.id.img_laptop);
            view.setTag(viewHolder);

        }
        else{
            viewHolder = (ViewHolder) view.getTag();
        }
        SanPham sanPham= (SanPham) getItem(i);
        viewHolder.txttenlaptop.setText(sanPham.getTensanpham());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        viewHolder.txtgialt.setText("Giá :"+decimalFormat.format(arrayLap.get(i).getGiasanpham())+" Đ");
        viewHolder.txtmotalaptop.setMaxLines(2);//chi 2 dong
        viewHolder.txtmotalaptop.setEllipsize(TextUtils.TruncateAt.END);//...

        viewHolder.txtmotalaptop.setText(arrayLap.get(i).getMotasanpham());
        Picasso.get().load(arrayLap.get(i).getHinhanhsanpham()).into(viewHolder.imglt);//load tu urrl
        return view;


    }
}
