package com.example.cuhngthietbi.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cuhngthietbi.R;
import com.example.cuhngthietbi.model.SanPham;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class DienThoaiAdapter extends BaseAdapter {
    Context context;
    ArrayList<SanPham> arrayPhone;

    public DienThoaiAdapter(Context context, ArrayList<SanPham> arrayPhone) {
        this.context = context;
        this.arrayPhone = arrayPhone;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public ArrayList<SanPham> getArrayPhone() {
        return arrayPhone;
    }

    public void setArrayPhone(ArrayList<SanPham> arrayPhone) {
        this.arrayPhone = arrayPhone;
    }

    @Override
    public int getCount() {
        return arrayPhone.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayPhone.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }
    public class ViewHolder{
        public TextView txttendienthoai,txtgiadt,txtmotadienthoai;
        public ImageView imgdt;


    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(viewHolder ==  null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.dong_dienthoai,null);
            viewHolder.txttendienthoai = view.findViewById(R.id.tv_dienthoai);
            viewHolder.txtgiadt = view.findViewById(R.id.tv_giadienthoai);
            viewHolder.txtmotadienthoai = view.findViewById(R.id.tv_motadienthoai);
            viewHolder.imgdt = view.findViewById(R.id.img_dienthoai);
            view.setTag(viewHolder);

        }
        else{
            viewHolder = (ViewHolder) view.getTag();
        }
        SanPham sanPham= (SanPham) getItem(i);
        viewHolder.txttendienthoai.setText(sanPham.getTensanpham());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        viewHolder.txtgiadt.setText("Giá :"+decimalFormat.format(arrayPhone.get(i).getGiasanpham())+" Đ");
        viewHolder.txtmotadienthoai.setMaxLines(2);
        viewHolder.txtmotadienthoai.setEllipsize(TextUtils.TruncateAt.END);

        viewHolder.txtmotadienthoai.setText(arrayPhone.get(i).getMotasanpham());
        Picasso.get().load(arrayPhone.get(i).getHinhanhsanpham()).into(viewHolder.imgdt);
        return view;


    }

}
