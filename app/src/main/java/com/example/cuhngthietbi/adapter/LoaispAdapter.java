package com.example.cuhngthietbi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cuhngthietbi.R;
import com.example.cuhngthietbi.model.Loaisp;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class LoaispAdapter extends BaseAdapter {
    ArrayList<Loaisp> arrayListloaisp;
    Context context;

    public LoaispAdapter(ArrayList<Loaisp> arrayListloaisp, Context context) {
        this.arrayListloaisp = arrayListloaisp;
        this.context = context;
    }

    @Override
    public int getCount() {//so luong
        return arrayListloaisp.size();
    }

    @Override
    public Object getItem(int i) {//lay tung thuoc tinh
        return arrayListloaisp.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //load nhanh du lieu
    public class ViewHolder{
        TextView txttenoaisp;
        ImageView imgloaisp;

    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
         if(view == null)
         {
             viewHolder = new ViewHolder();
             LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);// get dc service la layout cua we ra
             view = inflater.inflate(R.layout.dong_listview_loaisp,null);
            //Anh xa
             viewHolder.txttenoaisp= view.findViewById(R.id.textviewloaisp);
             viewHolder.imgloaisp= view.findViewById(R.id.imageviewoaisp);
             //gan vao ViewHolder
             view.setTag(viewHolder);


         }
         else {
             viewHolder = (ViewHolder) view.getTag();

         }
        Loaisp loaisp = (Loaisp) getItem(i);
        viewHolder.txttenoaisp.setText(loaisp.getTenloaisp());
        Picasso.get().load(loaisp.getHinhanhloaisp()).into(viewHolder.imgloaisp);//change


        return view;
    }
}
