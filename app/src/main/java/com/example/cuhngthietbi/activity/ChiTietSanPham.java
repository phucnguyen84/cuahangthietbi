package com.example.cuhngthietbi.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.cuhngthietbi.R;
import com.example.cuhngthietbi.adapter.GioHangAdapter;
import com.example.cuhngthietbi.model.GioHang;
import com.example.cuhngthietbi.model.SanPham;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ChiTietSanPham extends AppCompatActivity {
    Toolbar toolbar;
    ImageView img;
    TextView txtten,txtgia,txtmota;
    Spinner spinner;
    TextView mua;
    int id = 0,gia=0,idsp=0;
    String tensp,hinhanh,mota;
    GioHangAdapter gioHangAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_san_pham);
        anhXa();
        actionToolbar();
        getInfor();
        catchEventSpinner();
        addEvent();

    }

//    private void catchEventButton() {
//        mua.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(MainActivity.arrCart.size()>0){
//                    int sluong = Integer.parseInt(spinner.getSelectedItem().toString());
//                    boolean out = false;
//                    for(int i = 0 ; i < MainActivity.arrCart.size();i++){
//                        if(MainActivity.arrCart.get(i).getId() == id){
//                            MainActivity.arrCart.get(i).setSl(MainActivity.arrCart.get(i).getSl()+ sluong);
//
//                        }
//                        MainActivity.arrCart.get(i).setGia(gia* MainActivity.arrCart.get(i).getSl());
//                        out = true;
//                    }
//                    if(out==false){
//                        int sl = Integer.parseInt(spinner.getSelectedItem().toString());
//                        long giaMoi = sl * gia;
//                        MainActivity.arrCart.add(new GioHang(id,sl,giaMoi,tensp,hinhanh));
//                        gioHangAdapter.notifyDataSetChanged();
//                    }
//
//                }
//
//                else{
//                    int sluong = Integer.parseInt(spinner.getSelectedItem().toString());
//                    long giaMoi = sluong * gia;
//                    MainActivity.arrCart.add(new GioHang(id,sluong,giaMoi,tensp,hinhanh));
//                    gioHangAdapter.notifyDataSetChanged();
//                }
//                Intent it = new Intent(getApplicationContext(), com.example.cuhngthietbi.activity.GioHang.class);
//                startActivity(it);
//            }
//        });
//    }
    //
    private void addEvent() {
        mua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MainActivity.arrCart.size()>0){
                    int sl= Integer.parseInt(spinner.getSelectedItem().toString());
                    boolean exsist=false;
                    for(int i=0;i<MainActivity.arrCart.size();i++){
                        if(MainActivity.arrCart.get(i).getId()==id){
                            MainActivity.arrCart.get(i).setSl(MainActivity.arrCart.get(i).getSl()+sl);
                            if(MainActivity.arrCart.get(i).getSl()>=10){
                                MainActivity.arrCart.get(i).setSl(10);
                            }
                            MainActivity.arrCart.get(i).setGia(gia*sl+MainActivity.arrCart.get(i).getGia());
                            exsist=true;
                        }
                    }
                    if(exsist==false){
                        int soluong= Integer.parseInt(spinner.getSelectedItem().toString());
                        long giamoi=soluong*gia;
                        MainActivity.arrCart.add(new GioHang(id,soluong,giamoi,tensp,hinhanh));
                        gioHangAdapter.notifyDataSetChanged();
                    }
                }else{
                    int soluong= Integer.parseInt(spinner.getSelectedItem().toString());
                    long giamoi=soluong*gia;
                    //public GioHang(int id, int sl, long gia, String ten, String hinh) {
                    MainActivity.arrCart.add(new GioHang(id,soluong,giamoi,tensp,hinhanh));
                    gioHangAdapter.notifyDataSetChanged();
                }
                Intent intent=new Intent(getApplicationContext(), com.example.cuhngthietbi.activity.GioHang.class);
                startActivity(intent);
            }
        });
    }

    //

    private void catchEventSpinner() {
        Integer [] sl = new Integer[]{1,2,3,4,5,6,7,8,9,10};
        ArrayAdapter<Integer> arr = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_dropdown_item,sl);
        spinner.setAdapter(arr);

    }

    //
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menugiohang:
                Intent it = new Intent(getApplicationContext(),com.example.cuhngthietbi.activity.GioHang.class);
                startActivity(it);

        };

        return true;    }
    //

    private void getInfor() {

        SanPham sp = (SanPham) getIntent().getSerializableExtra("thongtinsanpham");
        id = sp.getId();
        gia = sp.getGiasanpham();
        idsp = sp.getIdsanpham();
        tensp = sp.getTensanpham();
        hinhanh= sp.getHinhanhsanpham();
        mota= sp.getMotasanpham();
        txtten.setText(tensp);
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        txtgia.setText("Giá: "+ decimalFormat.format(gia)+"VND");
        txtmota.setText(mota);
        Picasso.get().load(hinhanh).into(img);


    }


    private void actionToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();


            }
        });
    }

    private void anhXa() {
        toolbar= findViewById(R.id.toolbar_chitietsanpham);
        img= findViewById(R.id.img_chitietsanpham);
        txtten= findViewById(R.id.tv_tenchitietsanpham);
        txtgia= findViewById(R.id.tv_giachitietsanpham);
        txtmota= findViewById(R.id.tv_motachitietsanpham);
        spinner= findViewById(R.id.spinner);
        mua= findViewById(R.id.btn_datmua);
        gioHangAdapter=new GioHangAdapter(ChiTietSanPham.this,MainActivity.arrCart);

    }
}
