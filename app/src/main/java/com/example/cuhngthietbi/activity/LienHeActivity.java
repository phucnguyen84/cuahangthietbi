package com.example.cuhngthietbi.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;

import com.example.cuhngthietbi.R;

public class LienHeActivity extends AppCompatActivity {

    Toolbar tblienhe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lien_he);
        tblienhe=findViewById(R.id.tb_lienhe);
        actionBar();
    }

    private void actionBar() {
        setSupportActionBar(tblienhe);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tblienhe.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
