package com.example.cuhngthietbi.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.cuhngthietbi.R;
import com.example.cuhngthietbi.adapter.GioHangAdapter;
import com.example.cuhngthietbi.ultil.CheckConnection;

import java.text.DecimalFormat;

public class GioHang extends AppCompatActivity {

    ListView lvGiohang;
    public static TextView tvThongbao,tvTongtien;
    Button btnThanhToan,btnTieptucmua;
    Toolbar toolbarGiohang;
    public static GioHangAdapter gioHangAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gio_hang);
        anhXa();
        actionToolbar();
        catchEvent();
        paymentAndNavigate();

    }

    private void paymentAndNavigate() {
        btnTieptucmua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(it);
            }
        });
        btnThanhToan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MainActivity.arrCart.size() > 0){
                    Intent it = new Intent(getApplicationContext(),ThongTinKH.class);
                    startActivity(it);
                }
                else{
                    CheckConnection.showToast_Short(getApplicationContext(),"Giỏ hàng trống");
                }

            }
        });
    }
//

//
    public static void catchEvent() {
        long tt=0;
        for(int i = 0 ; i < MainActivity.arrCart.size();i++){
            tt+=MainActivity.arrCart.get(i).getGia();
            DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
            tvTongtien.setText(decimalFormat.format(tt)+"VND");
        }
    }



    private void actionToolbar() {
        setSupportActionBar(toolbarGiohang);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarGiohang.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void anhXa() {
        lvGiohang= (ListView) findViewById(R.id.lv_giohang);
        tvThongbao= (TextView) findViewById(R.id.tv_thongbao);
        tvTongtien= (TextView) findViewById(R.id.tv_tongtien);
        btnThanhToan= (Button) findViewById(R.id.btn_thanhtoangiohang);
        btnTieptucmua= (Button) findViewById(R.id.btn_tieptucmuahang);
        toolbarGiohang= (Toolbar) findViewById(R.id.toolbar_giohang);
        gioHangAdapter=new GioHangAdapter(GioHang.this,MainActivity.arrCart);
        lvGiohang.setAdapter(gioHangAdapter);

    }
}
