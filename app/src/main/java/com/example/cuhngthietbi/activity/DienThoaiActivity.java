package com.example.cuhngthietbi.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.cuhngthietbi.R;
import com.example.cuhngthietbi.adapter.DienThoaiAdapter;
import com.example.cuhngthietbi.model.SanPham;
import com.example.cuhngthietbi.ultil.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DienThoaiActivity extends AppCompatActivity {
    Toolbar toolbar;
    ListView listView;
    DienThoaiAdapter dt;
    ArrayList<SanPham> mangdt;
    int idloaisp=0;
    int page =1 ;
    View footer;
    boolean isloading= false;
    mHandler mHandler;
    boolean limit = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dien_thoai);
        anhXa();
        getIdloaisp();
        actionToolbar();
        getData(page);
        loadMore();
    }

    private void loadMore() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                Intent it = new Intent(getApplicationContext(),ChiTietSanPham.class);
                it.putExtra("thongtinsanpham",mangdt.get(i));
                startActivity(it);
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {


            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int first, int visible, int totalItem) {
                if(first + visible == totalItem  && totalItem != 0 && isloading == false && limit==false){//

                    isloading= true;
                    Thread th = new ThreadData();
                    th.start();


                }

            }
        });
    }

    private void getData(int page) {
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());//request
        String dd = Server.duongdandienthoai+page;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, dd, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listView.removeFooterView(footer);

                int id = 0;
                String tendt = "",hanh="",mta="";
                int gia=0;
                int idsp= 0;
                if(response!= null && response.length() !=2){
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        for(int i = 0 ; i < jsonArray.length();i++){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            id = jsonObject.getInt("id");
                            tendt = jsonObject.getString("tensp");
                            gia = jsonObject.getInt("giasp");
                            hanh = jsonObject.getString("hinhanhsp");
                            mta = jsonObject.getString("motasp");

                            idsp = jsonObject.getInt("idsanpham");
                            mangdt.add(new SanPham(id,tendt,gia,hanh,mta,idsp));
                            dt.notifyDataSetChanged();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                        limit= true;
                    listView.removeFooterView(footer);



                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override//push len data seerver
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<String,String>();
                params.put("idsanpham",String.valueOf(idloaisp));


                return params;
            }
        };
        requestQueue.add(stringRequest);

    }
    //
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menugiohang:
                Intent it = new Intent(getApplicationContext(),com.example.cuhngthietbi.activity.GioHang.class);
                startActivity(it);

        };

        return true;    }


    private void actionToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();//go back();
            }
        });


    }

    private void getIdloaisp() {
        idloaisp = getIntent().getIntExtra("idloaisanpham",-1);

    }

    private void anhXa() {
        toolbar = findViewById(R.id.toolbar_dienthoai);
        listView = findViewById(R.id.lv_dienthoai);
        mangdt = new ArrayList<>();
        dt= new DienThoaiAdapter(getApplicationContext(),mangdt);
        listView.setAdapter(dt);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        footer = inflater.inflate(R.layout.progressbar,null);
        mHandler= new mHandler();




    }

    public class mHandler extends Handler{
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case 0 : listView.addFooterView(footer);break;
                case 1:

                    getData(++page);
                    isloading=false;
                    break;
            }
            super.handleMessage(msg);
        }

    }

    public class ThreadData extends Thread{
        @Override
        public void run() {
            mHandler.sendEmptyMessage(0);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Message message = mHandler.obtainMessage(1);//lk vs handler
            mHandler.sendMessage(message);
            super.run();

        }
    }
}
