package com.example.cuhngthietbi.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ViewFlipper;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.cuhngthietbi.R;
import com.example.cuhngthietbi.adapter.LoaispAdapter;
import com.example.cuhngthietbi.adapter.SanphamAdapter;
import com.example.cuhngthietbi.model.GioHang;
import com.example.cuhngthietbi.model.Loaisp;
import com.example.cuhngthietbi.model.SanPham;
import com.example.cuhngthietbi.ultil.CheckConnection;
import com.example.cuhngthietbi.ultil.Server;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {
    public static ArrayList<GioHang> arrCart ;
    Toolbar toolbar;
    RecyclerView recyclerView;
    NavigationView navigationView;
    ListView listView;
    DrawerLayout drawerLayout;
    ArrayList<Loaisp> mangloaisp;
    LoaispAdapter loaispAdapter;
    int id=0;
    String tenloaisp="",hinhanhloaisp="";
    ArrayList<SanPham> mangsp;
    SanphamAdapter spAdaoter;
@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhXa();
        if(CheckConnection.haveNetworkConnection(getApplicationContext())){
            ActionBar();//setaction cho tollbar
            GetDuLieuLoaisp();
            GetDuLieuspmsnhat();
            CatchOnItemListView()

            ;

        }
        else{
            CheckConnection.showToast_Short(getApplicationContext(),"CHECK CONNECT");
            finish();
        }
//        ActionBar();//setaction cho tollbar
//        ActionViewLipper();
    }

    //


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    switch (item.getItemId()){
        case R.id.menugiohang:
            Intent it = new Intent(getApplicationContext(),com.example.cuhngthietbi.activity.GioHang.class);
            startActivity(it);

    };

return true;    }

    //
    private void CatchOnItemListView() {
    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
            switch(i){
                case 0:
                    if(CheckConnection.haveNetworkConnection(getApplicationContext())){
                        Intent it = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(it);
                    }
                    else
                    {
                        CheckConnection.showToast_Short(getApplicationContext(),"Check connection");
                    }
                    //close menu
                    drawerLayout.closeDrawer(GravityCompat.START);
                    break;
                case 1:
                    if(CheckConnection.haveNetworkConnection(getApplicationContext())){
                        Intent it = new Intent(MainActivity.this, DienThoaiActivity.class);
                        it.putExtra("idloaisanpham",mangloaisp.get(i).getId());
                        startActivity(it);

                    }
                    else
                    {
                        CheckConnection.showToast_Short(getApplicationContext(),"Check connection");
                    }
                    //close menu
                    drawerLayout.closeDrawer(GravityCompat.START);
                    break;
                case 2:
                    if(CheckConnection.haveNetworkConnection(getApplicationContext())){
                        Intent it = new Intent(MainActivity.this, LaptopActivity.class);
                        it.putExtra("idloaisanpham",mangloaisp.get(i).getId());
                        startActivity(it);
                    }
                    else
                    {
                        CheckConnection.showToast_Short(getApplicationContext(),"Check connection");
                    }
                    //close menu
                    drawerLayout.closeDrawer(GravityCompat.START);
                    break;
                case 3:
                    if(CheckConnection.haveNetworkConnection(getApplicationContext())){
                        Intent it = new Intent(MainActivity.this, LienHeActivity.class);
                        startActivity(it);
                    }
                    else
                    {
                        CheckConnection.showToast_Short(getApplicationContext(),"Check connection");
                    }
                    //close menu
                    drawerLayout.closeDrawer(GravityCompat.START);
                    break;
                case 4:
                    if(CheckConnection.haveNetworkConnection(getApplicationContext())){
                        Intent it = new Intent(MainActivity.this, ThongTinActivity.class);
                        startActivity(it);
                    }
                    else
                    {
                        CheckConnection.showToast_Short(getApplicationContext(),"Check connection");
                    }
                    //close menu
                    drawerLayout.closeDrawer(GravityCompat.START);
                    break;
            }
        }
    });
    }

    private void GetDuLieuspmsnhat() {
        //doc data tu urrl
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Server.duongdansanphammoinhat, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if(response != null){
                    int ID= 0 ;
                    String tensanpham = "";
                    Integer giasanpham = 0;
                    String hinhanhsp = "";
                    String motasp = "";
                    Integer idsp = 0;
                    for (int i = 0 ; i< response.length();i++){
                        try {
                            JSONObject jsonObject = response.getJSONObject(i);
                            ID = jsonObject.getInt("id");
                            tensanpham = jsonObject.getString("tensanpham");
                            giasanpham= jsonObject.getInt("giasanpham");
                            hinhanhsp = jsonObject.getString("hinhanhsp");
                            motasp= jsonObject.getString("motasanpham");
                            idsp= jsonObject.getInt(("idsanpham"));
                            mangsp.add(new SanPham(ID,tensanpham,giasanpham,hinhanhsp,motasp,giasanpham));
                            spAdaoter.notifyDataSetChanged();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonArrayRequest);
    }

    private void GetDuLieuLoaisp() {
        mangloaisp.add(new Loaisp(0, "Trang Chính", "https://vietadsgroup.vn/Uploads/files/trangchu-la-gi.png"));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        //doc json nhanh gon hon
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Server.duongdanloaisp, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if(response != null){
                    for(int i = 0 ; i< response.length() ; i++){
                        try {
                            JSONObject jsonObject =  response.getJSONObject(i);
                            id = jsonObject.getInt("id");
                            tenloaisp=jsonObject.getString("tenloaisp");
                            hinhanhloaisp=jsonObject.getString("hinhanhloaisp");
                            mangloaisp.add(new Loaisp(id,tenloaisp,hinhanhloaisp));
                            loaispAdapter.notifyDataSetChanged();//update
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    mangloaisp.add(new Loaisp(0, "Liên Hệ", "http://capnuocbenthanh.com/images/dtlienhe_1.jpg"));
                    mangloaisp.add(new Loaisp(0, "Thông Tin", "http://kinhtevadubao.vn/uploads/images/news/1515687283_news_10383.jpg"));


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CheckConnection.showToast_Short(getApplicationContext(),error.toString());
                CheckConnection.showToast_Short(getApplicationContext(),"ERRORRRRR");

            }
        });
        requestQueue.add(jsonArrayRequest);//action
    }



    private void ActionBar() {
        setSupportActionBar(toolbar);// ho tro cho toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(android.R.drawable.ic_menu_sort_by_size);//change icon
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//bat event
                drawerLayout.openDrawer(GravityCompat.START);

            }
        });

    }

    private void anhXa() {
        toolbar = findViewById(R.id.toolbarmanhinhchinh);
        recyclerView = findViewById(R.id.recyclerview);
        navigationView = findViewById(R.id.navigationview);
        listView = findViewById(R.id.listviewmanhinhchinh);
        drawerLayout = findViewById(R.id.drawerLayout);
        mangloaisp= new ArrayList<>();
        loaispAdapter = new LoaispAdapter(mangloaisp,getApplicationContext());
        listView.setAdapter(loaispAdapter);
         mangsp = new ArrayList<>();
         spAdaoter = new SanphamAdapter(getApplicationContext(),mangsp);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position==0 ? 2:1;
            }
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(spAdaoter);
        if(arrCart!=null){

        }else{
            arrCart = new ArrayList<>();
        }


    }

}
